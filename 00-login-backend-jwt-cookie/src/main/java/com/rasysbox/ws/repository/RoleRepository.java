package com.rasysbox.ws.repository;

import com.rasysbox.ws.entity.ERole;
import com.rasysbox.ws.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
