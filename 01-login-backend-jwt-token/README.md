# JWT Authentication Token with H2-Embedded or MySQL database

- BD en H2-Embedded Memory or MySQL
## Test-0

- Method: GET
- Url: /api/v1/user

- Response:
- Body:
```json
{
    "path": "/api/v1/user",
    "error": "Unauthorized",
    "message": "Full authentication is required to access this resource",
    "status": 401
}
```
# Registrar un nuevo usuario:

- Method: POST 
- Url: /api/auth/signup

- Response:
- Body:
```json
{
    "username":"rasysbox",
    "email":"rasysbox@correo.com",
    "password":"Password",
    "role": [
        "rasysbox", 
        "admin"
    ]
}
```

```
Roles: 
    admin => Administrador
    mod   => Moderador
    user  => Usuario
```

# Inicio de sesion de un usuario (Token JWT)

- Method: POST 
- Url: /api/auth/signin
- Key: Authorization Bearer XXXXXXXX....

- Response:
- Body:
```json
{
    "username":"rasysbox",
    "password":"123456"
}
```

# Test-1 () 

- Method: GET
- Url: /api/v1/user
- Key: Authorization Bearer XXXXXXXX....

- Response: 

- Body:
```
User Content.
```
